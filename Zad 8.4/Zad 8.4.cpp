// Zad 8.4.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
volatile DWORD dwStart, result_s, result_w, suma_s, suma_w, min_s, min_w, max_s, max_w;

double silnia_iter(int n)
{
	double silnia = 1;
	for (double i = 1; i <= n; i++)
	{
		silnia = silnia * i;
	}
	return silnia;
}
double e_s(int num_steps)
{
	int i;
	double x, e = 0;
	for (i = 0; i < num_steps; i++)
	{
		x = 1 / silnia_iter(i);
		e = e + x;
	}
	return e;
}
double e_w(int num_steps) 
{
	int i;
	double x, e = 0;
#pragma omp parallel for reduction(+:e) private(x)
	for (i = 0; i < num_steps; i++)
	{
		x = 1 / silnia_iter(i);
		e = e + x;
	}
	return e;
}
int main(int argc, char* argv[])
{
	double d, min_ratio, max_ratio, suma_ratio, ratio;
	ratio = 0;
	int i, n = 10000;

	for (i = 0; i < 10; i++)
	{
		dwStart = GetTickCount();
		d = e_s(n);
		result_s = GetTickCount() - dwStart;
		printf_s("S: e = %.15f, czas: %d ms\n", d, result_s);
		if (i == 0)
		{
			suma_s = min_s = max_s = result_s;
		}
		else
		{
			if (result_s < min_s) min_s = result_s;
			if (result_s > max_s) max_s = result_s;
			suma_s += result_s;
		}

		dwStart = GetTickCount();
		d = e_w(n);
		result_w = GetTickCount() - dwStart;
		printf_s("W: e = %.15f, czas: %d ms\n", d, result_w);
		if (i == 0)
		{
			suma_w = min_w = max_w = result_w;
		}
		else
		{
			if (result_w < min_w) min_w = result_w;
			if (result_w > max_w) max_w = result_w;
			suma_w += result_w;
		}

		ratio = ((double)result_s / (double)result_w);
		printf_s("Przyspieszenie: %.2f\n\n", ratio);
		if (i == 0)
		{
			min_ratio = max_ratio = suma_ratio = ratio;
		}
		else
		{
			if (ratio < min_ratio) min_ratio = ratio;
			if (ratio > max_ratio) max_ratio = ratio;
			suma_ratio += ((double)result_s / (double)result_w);
		}
	}
	printf_s("Ilosc krokow: %d\n", n);
	printf_s("Ilosc pomiarow: %d\n\n", i);
	printf_s("Min czas sekwencyjny: %d\n", min_s);
	printf_s("Max czas sekwencyjny: %d\n", max_s);
	printf_s("Sredni czas sekwencyjny: %.2f\n\n", ((double)suma_s / (double)i));
	printf_s("Min czas wspolbiezny: %d\n", min_w);
	printf_s("Max czas wspolbiezny: %d\n", max_w);
	printf_s("Sredni czas wspolbiezny: %.2f\n\n", ((double)suma_w / (double)i));
	printf_s("Min przyspieszenie: %.2f\n", min_ratio);
	printf_s("Max przyspieszenie: %.2f\n", max_ratio);
	printf_s("Srednie przyspieszenie: %.2f\n\n", (suma_ratio / (double)i));

	system("pause");
	return 0;
}